# How to use Bitbucket Pipelines to build a Go (golang) project

## Stepping through [bitbucket-pipelines.yml](bitbucket-pipelines.yml)

### Setup the environment

A general-purpose [setup script](bitbucket-pipelines-go.sh) is used to setup the environment.

    source bitbucket-pipelines-go.sh

The setup script sets the IMPORT_PATH environment variable. An
[import path](https://golang.org/doc/code.html#ImportPaths) is a string that uniquely identifies a
package. The import path for this example project is built on two of
[Bitbucket Pipelines default environment variables](https://confluence.atlassian.com/display/BITBUCKET/Environment+variables+in+Bitbucket+Pipelines).

The setup script creates a package directory inside the Go workspace. We are using the latest tag of the
official [golang](https://hub.docker.com/_/golang/) docker image. This image uses the /go directory
as its Go workspace (and sets the GOPATH environment variable).

The setup script links the working directory to the package directory. Bitbucket Pipelines overrides
the working directory of the golang Docker image. We can rely on the fact that the working directory
contains our project's source code, and add a symbolic link from the Go workspace to the current
working directory. This allows us to build and install the program with the go tool.

### Compile and test `stringutil` library

Our very simple `stringutil` library has a dependency on
[xstrings](https://github.com/huandu/xstrings). The `go get` command is used to download this
dependency.

    cd ${IMPORT_PATH}/stringutil
    go get

Next this project's `stringutil` library is compiled with `go build`.

    go build

Finally, the `go test` command runs [stringutil/reverse_test.go](stringutil/reverse_test.go).

    go test


### Install and run the `hello` program

The `hello` program depends on the `stringutil` library. The `go install` command compiles it and
places the binary in `${GOPATH}/bin`. The golang Docker image has ${GOPATH}/bin on its `${PATH}`.

    cd ${IMPORT_PATH}/hello
    go install 
    hello


## References

* [How to Write Go Code](https://golang.org/doc/code.html)
* [Get started with Bitbucket Pipelines](https://confluence.atlassian.com/display/BITBUCKET/Get+started+with+Bitbucket+Pipelines)
